const bookContent = {
    1 : {
        "content": "<h1>The little prince</h1><p>by</p><p>Antoine de Saint-Exupéry</p>"
    },
    2: {
        "content": "<img src=\"images/prince.jpg\" alt=\"little-prince\"><div class=\"tape lower-corner\"></div><div class=\"tape higher-corner\"></div>"
    },
    3 : {
        "content": "<h1>Little prince</h1> <p>Little prince is a story about an adult discovering his true self as an inner child and the little prince itself to represent the inner child. He left his rose on his planet to explore the universe. He travelled to many planets, as well as, in which all adults have expressed their worst versions. It is a story of a brave journey of a little boy to find the real meaning of life, which match perfectibility  the educational content, since astronomy has been the journey of grasp new knowledge by looking into the stars, showing us our place in the universe with a scientific and philosophical meaning. This topic has fascinated children for many years.</p>"
    },
    4 : {
        "content": "<h1>The Little Prince's Cosmic Journey</h1><p>Once upon a time, I lived on a tiny planet, so small you could walk around it in a few steps. I met a rose with a thorny disposition, and she taught me about love and care. My journey began when I left my rose to explore the universe.</p><img class=\"snap\" id=\"sun\" src=\"images/sun.png\" alt=\"sun-snapshot\"><div class=\"tape lower-corner\"></div><p>In space exploration, humans venture far from home to discover the unknown, just as I left my tiny planet to explore the vast cosmos. They seek knowledge and encounter strange new worlds, just as I encountered unique and quirky inhabitants on my travels.</p>"
    },
    5 : {
        "content": "<p>As I traveled from planet to planet, I met a king who ruled over nothing but himself and a vain man who craved admiration. Their desires were vast, yet their realms were empty. Similarly, in space exploration, astronauts journey to distant planets and moons, sometimes finding desolate landscapes devoid of life.</p><p>I realized that true wealth and beauty come from within, not from the adoration of others or the accumulation of possessions. Space exploration teaches us the importance of humility in the face of the grandeur of the universe.</p><img class=\"snap\" id=\"earth\" src=\"images/planet3-1.png\" alt=\"sun-snapshot\"><div class=\"tape lower-corner\"></div>"
    },
    6 : {
        "content": "<p>On my journey, I met a lamplighter who tirelessly lit and extinguished his lamp as his planet spun in endless daylight. His dedication inspired me to understand the value of responsibility. Similarly, astronauts on space missions must meticulously perform their tasks to ensure the safety of their missions and the success of their endeavors.</p><img class=\"snap\" id=\"venus\" src=\"images/planet2-1.png\" alt=\"venus-snapshot\"><div class=\"tape lower-corner\"></div><p>In space exploration, responsibility and precision are essential qualities that enable humans to navigate the challenges of the cosmos, just as they were vital for my encounters with various inhabitants on my journey.</p>"
    },
    7 : {
        "content": "<p>One day, I met a geographer who dreamed of exploring the world but never left his desk. He taught me that true understanding comes from experiencing the world firsthand. In space exploration, scientists and explorers venture into the unknown to gather firsthand knowledge of distant planets and celestial bodies.</p><img class=\"snap\" id=\"staturn-v\" src=\"images/rocket-saturn-v.png\" alt=\"saturn-rocket-snapshot\"><div class=\"tape lower-corner\"></div><p>Both my story and space exploration emphasize the importance of direct experience and exploration to gain true understanding of the world around us.</p><img class=\"snap\" id=\"neptune\" src=\"images/planet7-1.png\" alt=\"neptune-snapshot\"><div class=\"tape lower-corner\"></div>"
    },
    8 : {
        "content": "<p>Finally, I encountered a fox who shared a profound secret: \"On ne voit bien qu'avec le cœur. L'essentiel est invisible pour les yeux.\" (One sees clearly only with the heart. What is essential is invisible to the eye.) This wisdom guided me throughout my journey, reminding me that the most important things in life cannot be seen or touched but are felt with the heart. In space exploration, scientists and astronauts also seek to understand the essence of the universe, delving into the mysteries that lie beyond the visible.</p><p>As my journey came to an end and I returned to my tiny planet, I realized that, like space explorers, I had gained a deeper understanding of the world and the people in it. Both my story and the quest for the stars show that curiosity, compassion, humility, responsibility, and the ability to see with the heart are qualities that connect us all in our explorations of the cosmos and the human experience.</p>"
    },
}

export default bookContent;