// Loader effect
function show_loader() {
    $('#loader').hide();
    $('#content').show();
    $('#index-body').css("background-image","url(./images/background.jpg)");
}
function show_content(){
    $('#content').show();
    $('body').css("background-image","url(./images/background.jpg)");
}

// Flying rocket index page
function triggerRelocation(link){
    if(window.location.pathname.includes("index")){
        $('.rocket').css("animation","rocket-move 8s");
        $('.little-prince').css("animation","prince-fade-out 3.5s");
        $('.little-prince').css("visibility","hidden");
        setTimeout(function(){
            window.location.href = link;
        }, 5400);
    } else{
        window.location.href = link;
    }
}

// Typing efect
var i = 0;
var txt = 'Welcome explorers to the amazing adventure in the space, scroll down to find more about this journey.';
var speed = 50;

function typeText() {
    $("#fade-main-text").show();
    const textElement = document.getElementById("fade-main-text");
    if (i < txt.length && textElement) {
        document.getElementById("fade-main-text").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeText, speed);
    }
}

// Scroll effect
var effect = true;
$(window).scroll(function(){
    if(effect){
        typeText();
        effect = false;
    }
});