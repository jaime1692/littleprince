const bodyElement = document.querySelector("body");
const spaceSection = document.getElementById("space");

const resizeObserver = new ResizeObserver((entries) => {
  for (const entry of entries) {
    if (entry.contentBoxSize) {
      const [{ blockSize },] = entry.contentBoxSize;
      spaceSection.style.height = `${blockSize}px`
    }
  }
});

resizeObserver.observe(bodyElement);
