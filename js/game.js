
// Game
var score = 0;
function move(typeMove){
    var valueText, value;
    getItems();
    getMeteors();
    if(typeMove == "up"){
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]);
        moving -= 20;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(90deg)");
        $('#avatar').css("transition","all 0.2s");
    }
    else if(typeMove == "down"){
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 20;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(-90deg)");
        $('#avatar').css("transition","all 0.2s");
    }
    else if(typeMove == "left"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]);
        moving -= 20;
        $('#avatar').css("left",moving+"px");
        $('#avatar').css("transform","rotate(0deg)");
        $('#avatar').css("transition","all 0.2s");
    }
    else if(typeMove == "right"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 20;
        $('#avatar').css("left",moving+"px");
        $('#avatar').css("transform","rotate(180deg)");
        $('#avatar').css("transition","all 0.2s");
    }
    else if(typeMove == "left-up"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]) - 10;
        $('#avatar').css("left",moving+"px");
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]) - 10;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(45deg)");
        $('#avatar').css("transition","transform 0.2s");
    }
    else if(typeMove == "right-up"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 10;
        $('#avatar').css("left",moving+"px");
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]) - 10;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(135deg)");
        $('#avatar').css("transition","transform 0.2s");
    }
    else if(typeMove == "left-down"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]) - 10;
        $('#avatar').css("left",moving+"px");
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 10;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(-45deg)");
        $('#avatar').css("transition","transform 0.2s");
    }
    else if(typeMove == "right-down"){
        valueText = $('#avatar').css("left");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 10;
        $('#avatar').css("left",moving+"px");
        valueText = $('#avatar').css("top");
        value = valueText.split("px");
        moving = parseInt(value[0]) + 10;
        $('#avatar').css("top",moving+"px");
        $('#avatar').css("transform","rotate(225deg)");
        $('#avatar').css("transition","transform 0.2s");
    }
}

function startGame(){

}

$("body").keypress(function(event) {
    if (gameover) { return;}
    var c = String.fromCharCode(event.which);
    if (c == 'w') {
        move('up');
    } else if (c == 'a') {
        move('left');
    } else if (c == 's') {
        move('down');
    } else if (c == 'd') {
        move('right');
    } else if (c == 'q') {
        move('left-up');
    } else if (c == 'e') {
        move('right-up');
    } else if (c == 'z') {
        move('left-down');
    } else if (c == 'x') {
        move('right-down');
    } 
});

function getItems(){
    var avatar = document.getElementById("avatar");
    var rectAvatar = avatar.getBoundingClientRect();
    var x = rectAvatar.left + (rectAvatar.right - rectAvatar.left)/2;
    var y = rectAvatar.bottom + (rectAvatar.top - rectAvatar.bottom)/2;
    var planets = document.getElementsByClassName("planets");
    for (var i = 0; i < planets.length; i++) {
        var rect = planets[i].getBoundingClientRect();
        if ((x <=  rect.right && x >= rect.left) && (y >= rect.top && y <= rect.bottom)) {
            score++;
            planets[i].style.display = "none";
            var point = document.getElementById("points");
            point.innerHTML = score + " points";
        }
    }
}

function getMeteors(){
    var avatar = document.getElementById("avatar");
    var rectAvatar = avatar.getBoundingClientRect();
    var x = rectAvatar.left + (rectAvatar.right - rectAvatar.left)/2;
    var y = rectAvatar.bottom + (rectAvatar.top - rectAvatar.bottom)/2;
    var meteors = document.getElementsByClassName("meteors");
    for (var i = 0; i < meteors.length; i++) {
        var rect = meteors[i].getBoundingClientRect();
        var rectX = rect.left + (rect.right - rect.left) / 2;
        var rectY = rect.bottom + (rect.top - rect.bottom) / 2;
        if (Math.abs(x - rectX) <=  50 && Math.abs(y - rectY) <= 50) {
            document.getElementById("time").innerHTML = "GAME OVER";
            document.getElementById("game").innerHTML = '<img id="avatar" src="images/explotion.png">';
            document.getElementById("avatar").style.top = rectAvatar.top + "px";
            document.getElementById("avatar").style.left = rectAvatar.left + "px";
            gameover = true;
        }
    }
}

var timeleft;
var gameover;
$( document ).ready(function() {
    timeleft = 120000;
    gameover = false;
});

setInterval(function() {
    if (!gameover) {
        var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
        if (seconds < 10) {
            document.getElementById("time").innerHTML = minutes + " : 0" + seconds;
        } else {
            document.getElementById("time").innerHTML = minutes + " : " + seconds;
        }
        timeleft -= 1000;
    } else {
        document.getElementById("time").innerHTML = "GAME OVER";
    }
}, 1000)