const LINKS_NAVBAR = [
    {source: "index", label: "Home"},
    {source: "explore", label: "Explore"},
    {source: "characters", label: "Characters"},
    {source: "story", label: "Story"},
    {source: "playground", label: "Playground"},
    {source: "about", label: "About"},
    {source: "contact", label: "Contact me"},
];

// Navegation bar
$("#nav-bar-component").ready(function() {
    const linksComponents = LINKS_NAVBAR.map(element => {
        const { source, label} = element;
        return `<li><a onclick=\"triggerRelocation('${source}.html')\">${label}</a></li>`;
    });

    $(`<nav class=\"nav-bar-main\">
        <a href=\"index.html\" class=\"nav-title\">Little prince <i class=\"fas fa-meteor\"></i> <strong>In a space adventure</strong></a>
        <ul class=\"nav-bar-links\">
            ${linksComponents}
        </ul>
    </nav>`).insertAfter("#nav-bar-component");
})

// Footer 
$("#footer-component").ready(function() {
    const linksComponents = LINKS_NAVBAR.map(element => {
        const { source, label} = element;
        return `<li><a href=\"${source}.html\">${label}</a></li>`;
    });

    $(`<footer class="footer-main">
        <section class="footer-main-links">
            <a href="index.html" class="nav-title">Little prince <i class="fas fa-meteor"></i> <strong>In a space adventure</strong></a>
            <ul class="nav-bar-links">
                ${linksComponents}
            </ul>
        </section>
        <section class="copyright">
            Copyright © 2019
        </section>
    </footer>`).insertAfter("#footer-component");
})
