import bookContent from "./../constants/bookContent.js"

// Loading DOM element
const loadingPageLeft = document.getElementById("loading-page-left");
const flipPageLeft = document.getElementById("flip-page-left");
const loadingPageRight = document.getElementById("loading-page-right");
const flipPageRight = document.getElementById("flip-page-right");
const INITIAL_PAGE = 1;

// inital book state
localStorage.setItem('bookPage', INITIAL_PAGE)
loadingPageLeft.style.visibility = 'hidden';
loadingPageRight.children[0].children[0].innerHTML = bookContent[INITIAL_PAGE].content


async function flipPage(element) {
    let count = localStorage.getItem('bookPage');
    if (!count) {
        localStorage.setItem('bookPage', 1);
    } else {
        if (element.includes('left')) {
            --count;
            localStorage.setItem('bookPage', --count);
            if (count <= 1) {
                loadingPageLeft.style.visibility = 'hidden';
            }
        } else {
            ++count;
            localStorage.setItem('bookPage', ++count);
            if (Object.keys(bookContent).length <= count) {
                loadingPageRight.style.visibility = 'hidden';
            }
        }
    }


    if (element.includes('left')) {
        const leftBackPage = flipPageLeft.children[0].children[1];
        const leftFrontPage = flipPageLeft.children[0].children[0];
        ++count;
        leftFrontPage.innerHTML = bookContent[count].content
        leftBackPage.innerHTML = bookContent[--count].content
        flipPageLeft.style.visibility = 'initial'
        if (count > 1) {
            loadingPageLeft.children[0].children[0].innerHTML = bookContent[--count].content
            loadingPageLeft.style.visibility = 'initial';
        }
    } else {
        const rightFrontPage = flipPageRight.children[0].children[0];
        const rigthBackPage = flipPageRight.children[0].children[1];
        --count
        rightFrontPage.innerHTML = bookContent[--count].content
        rigthBackPage.innerHTML = bookContent[++count].content
        flipPageRight.style.visibility = 'initial'
        if (Object.keys(bookContent).length > count) {
            loadingPageRight.children[0].children[0].innerHTML = bookContent[++count].content
            loadingPageRight.style.visibility = 'initial';
        }
    }

    const flipPage = document.getElementById(element);
    flipPage.classList.add('flip-page-animation')

    setTimeout(function(){
        if (element.includes('left')) {
            if (count <= 1) {
                loadingPageRight.children[0].children[0].innerHTML = bookContent[INITIAL_PAGE].content
            } else {
                loadingPageRight.children[0].children[0].innerHTML = bookContent[++count].content
            }
            loadingPageRight.style.visibility = 'initial';
        } else {
            if (count >= Object.keys(bookContent).length) {
                loadingPageLeft.children[0].children[0].innerHTML = bookContent[Object.keys(bookContent).length].content
            } else {
                loadingPageLeft.children[0].children[0].innerHTML = bookContent[--count].content
            }
            loadingPageLeft.style.visibility = 'initial';
        }

        flipPage.style.visibility = 'hidden';
        flipPage.classList.remove('flip-page-animation')
    }, 2000)
}

// Add flipping page listeners
loadingPageLeft.addEventListener("click", () => flipPage('flip-page-left'));
loadingPageRight.addEventListener("click", () => flipPage('flip-page-right'));